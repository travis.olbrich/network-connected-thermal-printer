# Network-Connected Thermal Printer

This is the source code for a simple webserver on a ESP-8266. Whatever is posted to an endpoing will be sent via serial to the printer. The goal is to have a few different services running on my network to have it print things like weather updates or other notifications.

## Hookup details
The TX line on the ESP-8266 is plugged in to the RX line of the thermal printer. The printer's TX line is not currently being used.

To keep the ESP-8266's start-up serial dump from reaching the printer and causing garbage to be printed a 555 timer and CD4066 IC are used to switch the serial connection.

## Remaining Work
- Document circuit design
- Support formatting of text as it is passed to the endpoint
- Authentication (and potential encryption) for endpoint