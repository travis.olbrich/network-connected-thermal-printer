#include "printer.h"
#include <Arduino.h>

void Printer::reset()
{
    // Init
    sendBytes(27, 64);

    // Heat time and interval
    sendBytes(27, 55, 7, 255, 5);

    // Density and timeout
    sendBytes(18, 35, (15 << 4 | 15));
}

void Printer::setAlignment(Centering option = Centering::LEFT)
{
    sendBytes(27, 97, (byte)option);
}

void Printer::setLeftPad(int spaces)
{
    if (spaces < 0)
        spaces = 0;
    if (spaces > 47)
        spaces = 47;

    sendBytes(27, 66, (byte)spaces);
}

void Printer::setSpacing(byte dots = 30)
{
    sendBytes(27, 51, dots);
}

void Printer::setFormatting(byte option = 0)
{
    sendBytes(27, 33, option);
}

void Printer::setUnderlineDots(byte dots)
{
    sendBytes(27, 45, dots);
}

void Printer::setUpdownMode(bool on)
{
    sendBytes(27, 123, on);
}

void Printer::setBackgroundMode(bool on)
{
    sendBytes(29, 66, on);
}

void Printer::setPrinterEnabled(bool on)
{
    sendBytes(27, 61, on);
}

void Printer::sendBytes(byte b0)
{
    Serial.write(b0);
    delay(100);
}

void Printer::sendBytes(byte b0, byte b1)
{
    Serial.write(b0);
    Serial.write(b1);

    delay(100);
}

void Printer::sendBytes(byte b0, byte b1, byte b2)
{
    Serial.write(b0);
    Serial.write(b1);
    Serial.write(b2);

    delay(100);
}

void Printer::sendBytes(byte b0, byte b1, byte b2, byte b3, byte b4)
{
    Serial.write(b0);
    Serial.write(b1);
    Serial.write(b2);
    Serial.write(b3);
    Serial.write(b4);

    delay(100);
}