#ifndef printer_h
#define printer_h

#include <Arduino.h>

class Printer
{
private:
public:
    static void sendBytes(byte b0);
    static void sendBytes(byte b0, byte b1);
    static void sendBytes(byte b0, byte b1, byte b2);
    static void sendBytes(byte b0, byte b1, byte b2, byte b3, byte b4);

    enum Centering
    {
        LEFT = 0,
        MIDDLE = 1,
        RIGHT = 2
    };

    enum Formatting
    {
        NONE = 0,
        EMPHASIZED = 1 << 3,
        DOUBLE_HEIGHT = 1 << 4,
        DOUBLE_WIDTH = 1 << 5,
        //DELETE_LINE = 1 << 6, // seems unsupported on my printer
        UNDERLINE = 1 << 7
    };

    static void reset();
    static void setAlignment(Centering option);
    static void setLeftPad(int spaces);
    static void setSpacing(byte dots);
    static void setFormatting(byte option);
    static void setUnderlineDots(byte dots);
    static void setUpdownMode(bool on);
    static void setBackgroundMode(bool on);
    static void setPrinterEnabled(bool on);
};

#endif