#ifndef printer_ota_h
#define printer_ota_h

class PrinterOta
{
public:
    static void setup();
    static void handle();
};

#endif