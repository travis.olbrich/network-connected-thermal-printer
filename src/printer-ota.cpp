#include "printer-ota.h"
#include <ArduinoOTA.h>

void PrinterOta::setup()
{
    ArduinoOTA.onStart([]() {
        Serial.println("Firmware updating.");
    });
    ArduinoOTA.onEnd([]() {
        Serial.println("End");
        digitalWrite(2, 0);
    });
    ArduinoOTA.onError([](ota_error_t error) {
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR)
        {
            Serial.println("Auth Failed");
        }
        else if (error == OTA_BEGIN_ERROR)
        {
            Serial.println("Begin Failed");
        }
        else if (error == OTA_CONNECT_ERROR)
        {
            Serial.println("Connect Failed");
        }
        else if (error == OTA_RECEIVE_ERROR)
        {
            Serial.println("Receive Failed");
        }
        else if (error == OTA_END_ERROR)
        {
            Serial.println("End Failed");
        }
    });
    ArduinoOTA.begin();
}

void PrinterOta::handle()
{
    ArduinoOTA.handle();
}