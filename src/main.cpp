#include <ESP8266WebServer.h>
#include "printer-ota.h"
#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <Arduino.h>
#include "printer.h"

#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h> 

ESP8266WebServer server(80);
WiFiManager wiFiManager;

int buttonState;
int lastButtonState = LOW;
unsigned long lastDebounceTime = 0;
unsigned long debounceDelay = 5000;

#define TRIGGER_PIN 0

void handlePlain()
{
  if (server.method() != HTTP_POST)
  {
    server.send(405, "text/plain", "Method Not Allowed");
  }
  else
  {
    server.send(200, "text/plain", "POST body was:\n" + server.arg("plain"));

    String msg = server.arg("plain");
    Printer::setPrinterEnabled(true);
    Serial.println(msg);
    Printer::setPrinterEnabled(false);
  }
}

void apConfigCallback (WiFiManager *wifi) {
  Printer::setPrinterEnabled(true);
  Serial.print("Connect to ");
  Serial.println(wifi->getConfigPortalSSID());
  Serial.println("to configure printer.\n\n");
  Printer::setPrinterEnabled(false);
}

void setup()
{
  pinMode(TRIGGER_PIN, INPUT);
  digitalWrite(TRIGGER_PIN, HIGH);

  delay(2000); // 555 configured for 2.2 second start-up delay, 2000ms should be enough

  Serial.begin(19200);

  Printer::reset();
  Printer::setPrinterEnabled(false);

  wiFiManager.setAPCallback(apConfigCallback);
  String mac = WiFi.macAddress();
  String ssid = "Printer " + String(ESP.getChipId());

  WiFi.hostname(ssid);
  wiFiManager.autoConnect(ssid.c_str());

  Printer::setPrinterEnabled(true);
  Serial.println("Printer ready on");
  Serial.print(WiFi.localIP());
  Serial.println("/printer/\n\n");
  Printer::setPrinterEnabled(false);

  PrinterOta::setup();

  server.on("/printer/", handlePlain);
  server.begin();
}

void loop()
{
  PrinterOta::handle();
  server.handleClient();

  int reading = digitalRead(TRIGGER_PIN);

  // If the switch changed, due to noise or pressing:
  if (reading != lastButtonState) {
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {

    if (reading != buttonState) {
      buttonState = reading;

      if (buttonState == LOW) {
        Printer::setPrinterEnabled(true);
        Serial.println("Resetting WiFi Credentials...\n\n");
        Printer::setPrinterEnabled(false);

        wiFiManager.resetSettings();
        ESP.restart();
      }
    }
  }
  lastButtonState = reading;

}
